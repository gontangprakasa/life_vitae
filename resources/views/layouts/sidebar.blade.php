<?php
	$currentAction = Route::currentRouteAction();
	list($controller, $method) = explode('@', $currentAction);
	$controller = str_replace("App\\Http\\Controllers\\", "", $controller);
?>
<section class="sidebar">
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="{{ ($controller=='HomeController'?'active':'') }}">
			<a href="{{ url('home') }}">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
			</a>
		</li>

		<hr>

		@if(in_array(Auth::User()->role_id, [1]))
            <li class="{{ ($controller=='CompanyController'?'active':'') }}">
                <a href="{{ route('company.index') }}">
                <i class="fa fa-building-o"></i> <span>Company</span>
                </a>
            </li>
            <li class="{{ ($controller=='EmployeeController'?'active':'') }}">
                <a href="{{ url('employee') }}">
                <i class="fa fa-user"></i> <span>Employee</span>
                </a>
            </li>
		@endif
		
	</ul>
</section>