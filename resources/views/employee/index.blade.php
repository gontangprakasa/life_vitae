@extends('layouts.admin')
@section('title', 'Employee')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('employee.create') }}"> Create New Employee</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Company Name</th>
            <th>Fullname</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Created At</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($employee as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $value->company_name }}</td>
            <td>{{ $value->first_name }} {{ $value->last_name }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->phone }}</td>
            <td>{{ $value->created_at }}</td>
            <td>
                <form action="{{ route('employee.destroy',$value->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('employee.show',$value->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('employee.edit',$value->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $employee->links() !!}
      
@endsection