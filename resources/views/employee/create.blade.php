@extends('layouts.admin')
@section('title', 'Add Employee')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('employee.index') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Warning!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <strong>First Name:</strong>
                <input type="text" name="first_name" class="form-control" value="{{ old('first_name') }}" placeholder="First Name">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <strong>Last Name:</strong>
                <input type="text" name="last_name" class="form-control" value="{{ old('last_name') }}" placeholder="Last Name">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <strong>Phone:</strong>
                <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" placeholder="Phone">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <strong>Company:</strong>
                <select name="company_id" id="company_id" class="form-control" required>
                    <option value="" hidden>Choose Company </option>
                    @foreach(App\Models\Company::pluck('name','id') as $id => $label)
                        <option value="{{ $id }}" >{{ $label }}</option>
                    @endforeach
                </select>
            </div>
        </div>
       
        <div class="col-xs-12 col-sm-12 col-md-8 text-left">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
      
@endsection