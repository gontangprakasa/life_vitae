@extends('layouts.admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<center><H2>Selamat Datang di Sistem {{ env("APP_NAME") }}</H2></center>
		
		<div class="container">
			<div class="row">
			
				<div class="row text-center">
					<br><br><br><br><br>
					<div class="row">
						<div class="col-sm-6">
							<div class="card">
							<div class="card-body">
								<h2 class="card-title">Company</h2>
								<a href="#" class="btn btn-primary">{{ $count_employee }}</a>
							</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="card">
							<div class="card-body">
								<h2 class="card-title">Employee</h2>
								<a href="#" class="btn btn-primary">{{ $count_company }}</a>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	</div>
</div>
@endsection
