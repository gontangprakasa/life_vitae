@extends('layouts.admin')
@section('title', 'Edit')
@section('contentJs')
<script>
    $(document).ready(function(){
        $('#modal_contact').on('click', function() {
            var $el = $(this);
            var $link = $el.data('username');
            $(".modal-title").html($link);
            $(".show-image").attr("src",$link);
        });
    });
</script>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('company.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Warning!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('company.update',$company->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $company->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email:</strong>
                    <input type="text" name="email" value="{{ $company->email }}" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Logo:</strong>
                    <input type="file" name="logos" class="form-control" accept="image/*">
                    <a id="modal_contact" data-userName="{{ $company->logo }}" data-toggle="modal" href="#option_contact">
                        <i class="fa fa-check-square-o"></i> Show Image
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Website:</strong>
                    <input type="text" name="website" value="{{ $company->website }}" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-left">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>

    <div class="modal fade" id="option_contact" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" class="close" data-dismiss="modal"
                    type="button"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body text-center">
                    <img src='' width="300px" height="300px" class="img-fluid cursor-pointer show-image">
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@endsection