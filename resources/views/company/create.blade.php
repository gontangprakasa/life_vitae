@extends('layouts.admin')
@section('title', 'Add Company')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('company.index') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Warning!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('company.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Name">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <strong>Logo:</strong>
                <input type="file" name="logos" class="form-control" accept="image/*">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="form-group">
                <strong>Website:</strong>
                <input type="text" name="website" class="form-control" value="{{ old('website') }}" placeholder="Website">
            </div>
        </div>
       
        <div class="col-xs-12 col-sm-12 col-md-8 text-left">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
      
@endsection