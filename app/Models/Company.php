<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use HasFactory, SoftDeletes;
    
    public $timestamps = true;
    protected $fillable = [
        "name",
        "email", 
        "logo",
        "website", 
        "created_by",
        "updated_by"
    ];

    public function employee(){ 
        return $this->hasMany('App\Models\Employee','company_id')
        ->join('users', 'users.id', 'employees.user_id')
        ->whereNull('deleted_at');
    }
}
