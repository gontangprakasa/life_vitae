<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\User;
use App\Models\Role;

use Auth;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::select([
            'employees.*',
            'users.email',
            'companies.name as company_name'
        ])->join('users','users.id','employees.user_id')
        ->join('companies','companies.id','employees.company_id')
        ->whereNull('users.deleted_at')
        ->latest()->paginate(10);
    
        return view('employee.index',compact('employee'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
     
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'email'         => 'sometimes|email|unique:users,email,NULL,id,deleted_at,NULL',
            'company_id'    => 'required|exists:companies,id',
            'phone'         => 'sometimes',
        ]);

        $user               = new User();
        $user->email        = $request->input('email');
        $user->password     = bcrypt('password');
        $user->role_id      = Role::where('role_name', 'employee')->first()->id ?? 1;
        $user->created_by   = Auth::user()->id;
        if($user->save()) {
            $request['user_id']     = $user->id;
            $request['created_by']  = Auth::user()->id;
            Employee::create($request->all());
        }
     
        return redirect()->route('employee.index')
                        ->with('success','Employee created successfully.');
    }
     
    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $employee = $employee->select([
            'employees.*',
            'users.email',
            'companies.name as company_name'
        ])
        ->join('users','users.id','employees.user_id')
        ->join('companies','companies.id','employees.company_id')
        ->first();

        return view('employee.show',compact('employee'));
    } 
     
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $employee = $employee->select([
            'employees.*',
            'users.email',
            'companies.name as company_name'
        ])
        ->join('users','users.id','employees.user_id')
        ->join('companies','companies.id','employees.company_id')
        ->first();
        return view('employee.edit',compact('employee'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $user               = User::where('id',$employee->user_id)->first();
        $request->validate([
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'email'         => "sometimes|email|unique:users,email,$user->id,id,deleted_at,NULL",
            'company_id'    => 'required|exists:companies,id',
            'phone'         => 'sometimes',
        ]);

        $user->email        = $request->input('email');
        $user->updated_by   = Auth::user()->id;
        if($user->save()) {
            $request['user_id']     = $user['id'];
            $request['created_by']  = Auth::user()->id;
            $request['updated_by']  = Auth::user()->id;
            $employee->update($request->all());
        }
    
        return redirect()->route('employee.index')
                        ->with('success','Employee updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $user = User::where('id', $employee->user_id);
        $user->delete();
    
        return redirect()->route('employee.index')
                        ->with('success','Employee deleted successfully');
    }

}
