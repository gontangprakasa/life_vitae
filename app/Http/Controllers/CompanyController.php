<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\Company;
use Auth;

class CompanyController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::latest()->paginate(10);
    
        return view('company.index',compact('company'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }
     
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => "required|string",
            'email'     => 'sometimes|email|unique:companies,email,NULL,id,deleted_at,NULL',
            'logos'     => "nullable|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=150,min_height=150",
            'website'   => "sometimes|url",
        ]);

        $dir_upload = '/uploads/company/';
        $dir        = base_path().'/public'.$dir_upload;
        if (! File::exists($dir)) {
            File::makeDirectory($dir,0777,true);
        }

        if($request->hasFile('logos')){
            $file = $request->file('logos');
            $filename = $dir_upload.$request['name'].'.'.$file->getClientOriginalExtension();
            $file->move($dir, $filename);
            $request['logo'] = $filename;
        }
        $request['created_by'] = Auth::user()->id;

        Company::create($request->all());
     
        return redirect()->route('company.index')
                        ->with('success','Company created successfully.');
    }
     
    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $company = Company::with(['employee'])->where('id', $company->id)->first();
        return view('company.show',compact('company'));
    } 
     
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('company.edit',compact('company'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'name'      => "required|string",
            'email'     => "sometimes|email|unique:companies,email,$company->id,id,deleted_at,NULL",
            'logos'     => "nullable|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=150,min_height=150",
            'website'   => "sometimes|url",
        ]);

        $dir_upload = '/uploads/company/';
        $dir        = base_path().'/public'.$dir_upload;
        if (! File::exists($dir)) {
            File::makeDirectory($dir,0777,true);
        }
        
        if($request->hasFile('logos')){
            $file = $request->file('logos');
            $filename = $dir_upload.$request['name'].'.'.$file->getClientOriginalExtension();
            $file->move($dir, $filename);
            $request['logo'] = $filename;
        }
        $request['updated_by'] = Auth::user()->id;
       
        $company->update($request->all());
    
        return redirect()->route('company.index')
                        ->with('success','Company updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
    
        return redirect()->route('company.index')
                        ->with('success','Company deleted successfully');
    }
}
