<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Employee;
use App\Models\Company;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count_employee = Employee::join('users','users.id','employees.user_id')->whereNull('deleted_at')->count();
        $count_company = Company::whereNull('deleted_at')->count();
        return view('home',compact([
            'count_employee',
            'count_company'
        ]));
    }
}
