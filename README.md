# Life Vitae Test Case
Company Management System.

# Daftar Isi
* [Prakata](#prakata)
* [Feature](#feature)
    * [Configuration](#configuration)
    * [Database](#database)
* [How to ryn](#how-to-run)
    * [Prerequisites](#prerequisites)
    * [Preparation](#preparation)
    * [Run The Project](#run-the-project)
* [ERD](#erd)

# Prakata
In application development, a basic structure that can be agreed upon and learned is needed
by all members of the development team. So it was created using the framework [Laravel](https://laravel.com/) This is to meet the needs.

# Feature
The following are the features contained in this system.

### Configuration
So that the application can run with certain parameters, you should be able to take advantage of
management configuration. The following modules are used regarding configuration.
| Modul | Utility |
|-|-|
| [Laravel Config](https://laravel.com/docs/8.x/configuration) | configuration management |


### Database
This system utilizes a database management system (DBMS).
Utilization of this database is assisted by ORM (Object-Relational Mapping) with a series of modules
the following.
| Modul | Utility |
|-|-|
| [Laravel TypeORM](https://laravel.com/docs/8.x/queries) |  ORM |
| [MYSQL](https://lumen.laravel.com/docs/8.x/database) |connect to DBMS MYSQL |

# How to Run
Here are the things that need to be done so that this project can run.

## Prerequisites
This system requires other systems such as DBMS, database cache, etc. order process
preparation until running the project can run smoothly, here's what you should do
installed first.
| Another system | Utility |
|-|-|
| [Git](http://git-scm.com) |version management|
| [PHP v8.0](https://www.php.net/manual/en/install.php) | as the machine that runs the project |
| [Composer](https://getcomposer.org/) | managing modules in projects |
| [MYSQL Ver 14.14 Distrib 5.7.35](https://www.mysql.com/) | permanent data management |

## Preparation
Before the project can be run, there are several things that need to be prepared.
The following are the steps to prepare the project.

1. Clone this repository by running the following command in the terminal emulator.
```sh
git init
git clone https://gitlab.com/gontangprakasa/life_vitae.git
```
2. Go to the project directory by running the following command.
```sh
cd ./life_vitae
```
3. Install the modules needed by the project by running the following command.
```sh
# with composer
composer install and composer update
```

5. Create a file named `.env` and fill it with the desired configuration.
> An example of the contents of this `.env` file can be seen in file [.env.example](./.env.example)

4.Create a database according to the .env that has been set

6. Generate the required table by running the following command.
```sh
php artisan migrate
```

7. Running autoSeeder the required data by running the following command.
```sh
php artisan db:seed
```

## Run The Project
After the project is ready, it can then be run using the following command.
```sh
php artisan serve
```

# Entity Relationship Diagram
In this system, before doing development, it is necessary to have a business process or designed documentation.
To see the ERD that has been created, please open the file [documentation/](./documentation).

