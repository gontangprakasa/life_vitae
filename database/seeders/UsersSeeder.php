<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'role_id'           => 1,
            'email'             => 'admin@admin.com',
            'password'          => bcrypt('password'),
            'email_verified_at' => date('Y-m-d H:i:s'),
            'remember_token'    => NULL,
            'created_at'        => date('Y-m-d H:i:s'),
            'created_by'        => 1
        ]);
    }
}
