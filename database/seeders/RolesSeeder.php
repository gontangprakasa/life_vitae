<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $RoleItems = [
            ['role_name'    => 'Super Admin'],
            ['role_name'    => 'Employee'],
            ['role_name'    => 'Company'],
        ];

        foreach ($RoleItems as $RoleItem) {
            Role::insert([
                'role_name'     => $RoleItem['role_name'],
                'created_by'    => 1
            ]);
        }
    }
}
